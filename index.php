<?php
$token = 'xxx';
$apiUrl = 'https://xxx.bitrix24.ru/rest/1/xxx/';

if($_REQUEST['auth']['application_token'] !== $token){
    die();
}

if($_REQUEST['event'] !== 'ONCRMDEALUPDATE'){
    die();
}

$dealId = (int)$_REQUEST['data']['FIELDS']['ID'];
if($dealId <= 0){
    die();
}

$deal = json_decode(file_get_contents(
    $apiUrl.'crm.deal.get/?id='.$dealId
), true);

$newAssigned = (int)$deal['result']['ASSIGNED_BY_ID'];
// Тут можно посмотреть в кэш, и понять поменялся ли ответственный. Поможет не дергать задачи без надобности.
if($newAssigned <= 0){
    die();
}

// По максимуму урезаем выдачу. Нам не нужны задачи которые уже за новым ответственным. А вообще надеемся на обновление API.
$tasks = json_decode(file_get_contents(
    $apiUrl.'task.item.list/?ORDER[]=&FILTER[!RESPONSIBLE_ID]='.$newAssigned.'&PARAMS[]=&SELECT[]=TITLE&SELECT[]=UF_CRM_TASK'
    ), true);

foreach ($tasks['result'] as $task) {
    if(!is_array($task['UF_CRM_TASK'])){
        continue;
    }
    $findTask = false;
    foreach ($task['UF_CRM_TASK'] as $CRM_TASK) {
        if($CRM_TASK === 'D_'.$dealId){
            $findTask = true;
            break;
        }
    }
    if(!$findTask){
        continue;
    }

    file_get_contents(
        $apiUrl.'task.item.update/?TASKID='.$task['ID'].'&TASKDATA[RESPONSIBLE_ID]='.$newAssigned
    );
}